// pages/app.tsx
import "../styles/globals.css";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { AppProps } from "next/app";
import { ChakraProvider } from "@chakra-ui/react";

function MyApp({ Component, pageProps }: AppProps) {
  const [userName, setUserName] = useState("");
  const [roomName, setRoomName] = useState("");
  const router = useRouter();

  useEffect(() => {
    if (!roomName && !userName) {
      return;
    }
    let canceled = false;
    (() => {
      if (canceled) return;
      router.push(`/room/${roomName}`);
    })()

    return () => {
      canceled = true;
    }
    
  }, [roomName, userName])

  return (
    <ChakraProvider>
      <Component
        handleCredChange={(userName: string, roomName: string) => {
          setUserName(userName);
          setRoomName(roomName);
        }}
        userName={userName}
        roomName={roomName}
        {...pageProps}
      />
    </ChakraProvider>
  );
}

export default MyApp;
