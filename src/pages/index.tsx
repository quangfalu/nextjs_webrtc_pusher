import {
  Avatar,
  AvatarBadge,
  Box,
  Center,
  Flex,
  StackDivider,
  VStack,
  Text,
  Button,
} from "@chakra-ui/react";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useState } from "react";
import styles from "../styles/Home.module.css";

interface Props {
  handleCredChange: (userName: string, roomName: string) => void;
  handleLogin: () => void;
}

const data = [
  {
    id: 1,
    name: "kg01",
    type: 1,
  },
  {
    id: 2,
    name: "kg02",
    type: 1,
  },
];

export default function Home({ handleCredChange }: Props) {
  const [roomName, setRoomName] = useState("");
  const [userName, setUserName] = useState("");

  useEffect(() => {
    handleCredChange(userName, roomName);
  }, [roomName, userName, handleCredChange]);

  return (
    <div className={styles.container}>
      <Head>
        <title>
          Native WebRTC API with NextJS and Pusher as the Signalling Server
        </title>
        <meta
          name="description"
          content="Use Native WebRTC API for video conferencing"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <VStack
        divider={<StackDivider borderColor="gray.200" />}
        spacing={4}
        align="stretch"
        mt="10"
      >
        {data.map((item) => (
          <Flex
            align="center"
            key={item.id}
            onClick={() => {
              setUserName(item.name)
              setRoomName('kn01' + item.name)
            }}
          >
            <Box flex="1">
              <Avatar src="">
                <AvatarBadge
                  borderColor="#F0FFF4"
                  boxSize="1.25em"
                  bg="green.500"
                />
              </Avatar>
            </Box>
            <Box flex="9">
              <Text as="b" color="black">
                {item.name}
              </Text>
            </Box>
          </Flex>
        ))}
      </VStack>
    </div>
  );
}
